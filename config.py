import os


class BaseConfig:
    PORT = "8080"


class DevConfig(BaseConfig):
    pass


class ProdConfig(BaseConfig):
    pass


CONFIG_DICT = {
    'Dev': DevConfig,
    'Prod': ProdConfig
}


def get_config():
    env = os.getenv('ENV', 'Dev')
    return CONFIG_DICT[env]
