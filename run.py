from factory import create_app
from config import get_config
from app import socketio

app = create_app()
socketio.init_app(app)
if __name__ == "__main__":
    socketio.run(app, port=get_config().PORT)
