from flask_restful import Resource
from flask import request
from app import socketio


class Alerter(Resource):
    def get(self, ):
        message = request.form['message']
        print(message)
        socketio.emit('update', message)
