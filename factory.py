from flask_socketio import SocketIO
from flask import Flask
from config import get_config
from flask_restful import Api


def create_app():
    app = Flask(__name__, template_folder='./app/templates')
    app.config.from_object(get_config())
    api = Api(app)
    init_resorces(api)
    app.config['SECRET_KEY'] = 'mysecret'
    return app


def init_resorces(api: Api):
    from resources.alerter import Alerter
    from resources.index import Index
    api.add_resource(Alerter, '/Alerter', '/alert')
    api.add_resource(Index, '/Index', '/')
